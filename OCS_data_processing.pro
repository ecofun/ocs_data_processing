;===================================================================================
;///////////////////////////////////////////////////////////////////////////////////
;///////////////////////////////////////////////////////////////////////////////////
;
;       Main program to process data from lab soil OCS experiment 
;
;///////////////////////////////////////////////////////////////////////////////////
;///////////////////////////////////////////////////////////////////////////////////
;===================================================================================

PRO main, data_id=data_id, flag_demo=flag_demo

IF (size(data_id,/type) eq 0) then data_id = '160304_PILO_soil1_aero'
IF (size(flag_demo,/type) eq 0) then flag_demo = 0
;IF (size(data_id,/type) eq 0) then data_id = 'DATAID'
;flag_demo = 0

;===================================================================================
;///////////////////////////////////////////////////////////////////////////////////
; GENERAL FLAGS, CONSTANTS, HEADERS,...
;///////////////////////////////////////////////////////////////////////////////////
;===================================================================================

;--- default header
@ogee_general_header.inc
separator = ','
CCG_RGBLOAD,file=!DIR+'/lib/My_IDL/ccg_lib/data/color_comb1'

;--- i/o files
csv_file = 'data_files/concatenate_data' + dl + data_id + '.csv' ;--- concentration file
path_plots = 'pdf_files/IDL' + dl
date_id = (STR_SEP(data_id,'_'))[0]
soil_id = (STR_SEP(data_id,'_'))[0]

;--- general parameters
;--- NOTE: n_mean_min=1 NEEDED BECAUSE OTHERWISE WE ENDUP WITH
;    time_mean=missval WITH MORE RECENT SEQUENCES FOR SOME REASON...
n_mean = 30                     ;--- nb of records to compute a mean
n_mean_min = 15                 ;--- minimum nb of records to compute a mean
spline = 1                      ;--- to interpolate standards over time (spline or linear)
flag2_threshold = 3.            ;--- deviation threshold from the mean (in s.d. units) for flag2

;--- experiment-specific parameters
@data_id.inc
;@data_files/concatenate_data/160304_PILO_soil1_aero.inc
;chamber_type = 'glass'
;chamber_flow = 0.25                       ;--- lpm
;cal_values = ['zero1','NOAA1']            ;--- cal tank names
;cal_vici_id = [1,3]                       ;--- VICI valve value for cal tanks
;in_vici_id = 0                            ;--- VICI valve value for bypass (chamber inlet)
;out_vici_id = [9,10,11,12,13,14,15]       ;--- VICI valve value for chambers (chamber outlets)
;ab_vici_id = 2                            ;--- VICI valve value for auto-background
;blank_id = 15
;algae_id = [13,11,9]                     ;--- only for plots
;infirst = +1                              ;--- -1=out/in (3 times), +1=in/out (3 times)
;...
n_cals = n_elements(cal_vici_id)
n_outs = n_elements(out_vici_id)

;-- soil chamber parameters
case chamber_type of
   'teflon': soil_area = 0.00419 ;--- m2 (internal diameter = 7.20cm)
   'glass': soil_area  = 0.00615 ;--- m2 (internal diameter = 8.85cm)
endcase

;--- plot parameters
sec_per_page = 3600. ;--- 1 hour per page

;===================================================================================
;///////////////////////////////////////////////////////////////////////////////////
; STANDARD VALUES (OCS, OCS_sd, H2O, H2O_sd, CO2, CO2_sd)
;///////////////////////////////////////////////////////////////////////////////////
;===================================================================================

;--- describe ASCII file's structure
standards = {name:   [      'zero1',         'NOAA1',         'NOAA2',         'NOAA3'], $
             source: [     'Deuste',          'NOAA',          'NOAA',          'NOAA'], $
             CO2:    [          0.0,           400.6,           400.6,           400.6], $ ;--- ppm (CO2 unknown for NOAA2 and NOAA3) 
             CO2_sd: [          1.0,             0.2,             0.2,             0.2], $ ;--- ppm
             OCS:    [          0.0,           524.0,           319.9,           468.4], $ ;--- ppt
             OCS_sd: [          5.0,             3.0,             3.0,             4.2], $ ;--- ppt
             H2O:    [          0.0,            20.0,            20.0,            20.0], $ ;--- ppm (still unknown...)
             H2O_sd: [        100.0,           100.0,           100.0,           100.0], $ ;--- ppm
             color:  [            2,               7,               7,               7]}

;;--- resize structure to keep only the ones we are interested in
n_standard = n_elements(cal_values)
case n_standard of
   2: ii = WHERE(standards.name EQ cal_values[0] OR standards.name EQ cal_values[1])
   3: ii = WHERE(standards.name EQ cal_values[0] OR standards.name EQ cal_values[1]  OR standards.name EQ cal_values[2])
endcase
cmd = 'dummy = CREATE_STRUCT("'+(tag_names(standards))[0]+'",'+$
      'standards.'            +(tag_names(standards))[0]+'[ii])'
res = EXECUTE(cmd)
FOR j=1,n_elements(tag_names(standards))-1 DO BEGIN
   cmd = 'dummy = CREATE_STRUCT(dummy,"'+(tag_names(standards))[j]+'",'+$
         'standards.'                  +(tag_names(standards))[j]+'[ii])'
   res = EXECUTE(cmd)
ENDFOR
standards = dummy
;---
n_standard = n_elements(standards.name)
n_tracer = 3
tracer_name = (tag_names(standards))[2+indgen(n_tracer)*2]
tracer_title = 'raw ' + tracer_name
sd_target = [0.2,2.0,1.0]

;===================================================================================
;///////////////////////////////////////////////////////////////////////////////////
; READ DATA
;///////////////////////////////////////////////////////////////////////////////////
;===================================================================================

;--- read headers
OPENR,unit,csv_file,/GET_LUN
s = ''
READF,unit,s
var_name = STRCOMPRESS(STR_SEP(s,','),/REMOVE_ALL)
n_VAR = N_ELEMENTS(var_name)
FREE_LUN, unit
for j=0,n_var-1 do var_name[j] = STRSPLIT(var_name[j],'"',/EXTRACT)

;--- describe ASCII file's structure
fieldTypes = [7,replicate(5,12),7,replicate(5,5),7,7,replicate(5,4),7,7]
ii = where(var_name eq 'CO',nii)
if nii gt 0 then fieldTypes = [7,replicate(5,15),7,replicate(5,12),7,7,replicate(5,7),7,7]
data_template = {version:1.0, $
                 datastart:1L, $
                 delimiter:',', $
                 missingValue:missrec, $
                 commentSymbol:'#', $
                 fieldCount:n_VAR, $
                 fieldTypes:fieldTypes,$ ;2=int,3=lon,4=flt,5=dble,7=str
                 fieldNames:var_name, $
                 fieldLocations:indgen(n_VAR), $
                 fieldGroups:indgen(n_VAR)}

;--- read file and compute total nb of records
nlines2skip = 1
dummy = READ_ASCII( csv_file, $
                    DATA_START=nlines2skip, $
                    TEMPLATE=data_template)

;--- allocate file's data (removing autobackground values)
ii = where(dummy.VICI_W ne ab_vici_id, n_time)
for j=0,n_VAR-1 do begin
   cmd = var_name[j]+' = dummy.' +var_name[j]+'[ii]'
   res = execute(cmd)
endfor

;--- change units
Raw_OCS = COS*1e3           ;--- ppb -> ppt
Raw_CO2 = CO2/1e3           ;--- ppb -> ppm
Raw_H2O = H2O/1e4           ;--- 10-9 -> 10-5
Raw_CO2_2 = CO2_2/1e3       ;--- ppb -> ppm
;---
ii = where(var_name eq 'COS',nii)
if nii gt 0 then var_name[ii] = 'Raw_OCS'
ii = where(var_name eq 'CO2',nii)
if nii gt 0 then var_name[ii] = 'Raw_CO2'
ii = where(var_name eq 'H2O',nii)
if nii gt 0 then var_name[ii] = 'Raw_H2O'
ii = where(var_name eq 'CO2_2',nii)
if nii gt 0 then var_name[ii] = 'Raw_CO2_2'

;--- compute time variables
month = intarr(n_time)
day = intarr(n_time)
year = intarr(n_time)
hour = intarr(n_time)
mins = intarr(n_time)
secs = fltarr(n_time)
for i=0l,n_time - 1 do begin
   date = (STR_SEP(DateTime[i],' '))[0]
   time = (STR_SEP(DateTime[i],' '))[1]
   year[i] = (STR_SEP(date,'-'))[0]
   month[i] = (STR_SEP(date,'-'))[1]
   day[i] = (STR_SEP(date,'-'))[2]
   hour[i] = (STR_SEP(time,':'))[0]
   mins[i] = (STR_SEP(time,':'))[1]
   secs[i] = (STR_SEP(time,':'))[2]
endfor
time_in_sec = long((day-day[0])*double(one_day) + hour*3600d + mins*60d + secs)
date_time = JULDAY(month,day,year,hour,mins,secs)
IGOR_time = date_time - JULDAY(1,1,1904,0,0,0d0)*double(one_day)
var_name = [var_name,'time_in_sec','date_time','IGOR_time']
fieldTypes = [fieldTypes, 3, 5, 5]
n_var = n_var+3
t0 = time_in_sec[0]

;--- create record number per valve switch
ii_switch = where(VICI_W ne SHIFT(VICI_W,+1),n_switch)
if ii_switch[0] ne 0 then begin ;--- case if last VICI_W equals first VICI_W
   ii_switch = [0,ii_switch]
   n_switch = n_switch + 1
endif
ii = [ii_switch,n_time]
record_number = intarr(n_time)
for i=0,n_elements(ii)-2 do record_number[ii[i]:(ii[i+1]-1)] = indgen(ii[i+1]-ii[i])

;===================================================================================
;///////////////////////////////////////////////////////////////////////////////////
; COMPUTE MEANS FOR EACH VALVE SWITCH
;///////////////////////////////////////////////////////////////////////////////////
;===================================================================================

N_for_mean = intarr(n_switch)
line_name_mean = strarr(n_switch)
FOR i=0,n_VAR-1 DO BEGIN
   if fieldTypes[i] ne 7 then begin
      cmd = var_name[i]+'_mean = REPLICATE(double(missval),n_switch)'
      res = execute(cmd)
      cmd = var_name[i]+'_sdev = REPLICATE(double(missval),n_switch)'
      res = execute(cmd)
   endif
ENDFOR

ii = [ii_switch,n_time]
FOR i=0,n_switch-1 DO BEGIN
   jj = WHERE(time_in_sec LE (time_in_sec[ii[i+1]-1]) AND $
              time_in_sec GE (time_in_sec[ii[i+1]-1]-n_mean+1) AND $
              VICI_W EQ VICI_W[ii[i+1]-1], njj)
   N_for_mean[i] = njj
   ;---
   str = 'dummy'
   if VICI_W[ii_switch[i]] eq in_vici_id then str = 'in'
   for ical=0,n_cals-1 do if VICI_W[ii_switch[i]] eq cal_vici_id[ical] then str = cal_values[ical]
   for iout=0,n_outs-1 do if VICI_W[ii_switch[i]] eq out_vici_id[iout] then str = 'out';+autostring(iout)
   line_name_mean[i] = str
   ;---
   FOR k=0,n_VAR-1 DO BEGIN
      IF njj GT n_mean_min OR var_name[k] EQ 'time_in_sec' OR var_name[k] EQ 'date_time' OR var_name[k] EQ 'IGOR_time' THEN BEGIN
         if fieldTypes[k] ne 7 then begin
            cmd = var_name[k]+'_mean[i] =   MEAN('+var_name[k]+'[jj])'
            res = execute(cmd)
            cmd = var_name[k]+'_sdev[i] = STDDEV('+var_name[k]+'[jj]) / sqrt(njj)'
            res = execute(cmd)
         endif
      ENDIF
   ENDFOR
;;;
  ; print, line_name_mean[i] + ': ', round(Raw_OCS_mean[i]), ' = mean of [', Raw_OCS[jj], ']',format='(a,i4,a,60(i4,","),a)'
  ;print,'mean from ',IGOR_time[jj[0]]-t0,' to ',IGOR_time[jj[njj-1]]-t0,format='(a,i6,a,i6)'
  ;stop
;;;
ENDFOR

;dec2bin,4000-status,status_binary,/quiet
;acclimatation_flag = reform(status_binary[15-2,*])
;;--- decompose status flag and compute acclimation_flag and dark_flag
status_flag = 4000 - FIX(StatusW_mean)
;print,  status_flag[UNIQ(status_flag, SORT(status_flag))]
if (size(status_code,/type) eq 0) then status_code = ['Dark','HighCOS','Acclimation','FollowSwitch','SwitchHighToLow']
rest_sum = REPLICATE(0,n_switch)
dark_flag = REPLICATE(0,n_switch) ;--- in case status_code does not have 'Dark'
acclimation_flag = REPLICATE(0,n_switch) ;--- in case status_code does not have 'Acclimation'
for i = 0, 3 do begin
   flag = ((status_flag-rest_sum) mod 2^(i+1))
   if i eq (where(status_code eq 'Dark'       ))[0] then dark_flag        = flag / (2^i)        ;--- if dark, flag=1
   if i eq (where(status_code eq 'Acclimation'))[0] then acclimation_flag = flag / (2^i) ;--- if acclimation, flag=1
   rest_sum = rest_sum + flag
endfor

;===================================================================================
;///////////////////////////////////////////////////////////////////////////////////
; DO A TEMPORAL INTERPOLATION OF STANDARD VALUES
;///////////////////////////////////////////////////////////////////////////////////
;===================================================================================

;--- compute interpolation
Raw_OCS_std_interp = DBLARR(n_standard,n_switch)
Raw_CO2_std_interp = DBLARR(n_standard,n_switch)
Raw_H2O_std_interp = DBLARR(n_standard,n_switch)
Raw_OCS_std_interp_sdev = DBLARR(n_standard,n_switch)
Raw_CO2_std_interp_sdev = DBLARR(n_standard,n_switch)
Raw_H2O_std_interp_sdev = DBLARR(n_standard,n_switch)
FOR i=0,n_standard-1 DO BEGIN
   jj = WHERE(line_name_mean EQ standards.name[i] and Raw_OCS_mean ne missval,njj)
   Raw_OCS_std_interp[i,*]      = INTERPOL(Raw_OCS_mean[jj],time_in_sec_mean[jj],time_in_sec_mean,SPLINE=(spline AND njj GT 3))
   Raw_OCS_std_interp_sdev[i,*] = INTERPOL(Raw_OCS_sdev[jj],time_in_sec_mean[jj],time_in_sec_mean,SPLINE=(spline AND njj GT 3))
   ;---
   jj = WHERE(line_name_mean EQ standards.name[i] and Raw_CO2_mean ne missval,njj)
   Raw_CO2_std_interp[i,*]      = INTERPOL(Raw_CO2_mean[jj],time_in_sec_mean[jj],time_in_sec_mean,SPLINE=(spline AND njj GT 3))
   Raw_CO2_std_interp_sdev[i,*] = INTERPOL(Raw_CO2_sdev[jj],time_in_sec_mean[jj],time_in_sec_mean,SPLINE=(spline AND njj GT 3))
   ;---
   jj = WHERE(line_name_mean EQ standards.name[i] and Raw_H2O_mean ne missval,njj)
   Raw_H2O_std_interp[i,*]      = INTERPOL(Raw_H2O_mean[jj],time_in_sec_mean[jj],time_in_sec_mean,SPLINE=(spline AND njj GT 3))
   Raw_H2O_std_interp_sdev[i,*] = INTERPOL(Raw_H2O_sdev[jj],time_in_sec_mean[jj],time_in_sec_mean,SPLINE=(spline AND njj GT 3))
ENDFOR

;===================================================================================
;///////////////////////////////////////////////////////////////////////////////////
; CALIBRATE SAMPLE VALUES
;///////////////////////////////////////////////////////////////////////////////////
;===================================================================================

Calibrated_OCS_mean = REPLICATE(missval,n_switch)
Calibrated_OCS_sdev = Raw_OCS_sdev
Calibrated_CO2_mean = REPLICATE(missval,n_switch)
Calibrated_CO2_sdev = Raw_CO2_sdev
Calibrated_H2O_mean = REPLICATE(missval,n_switch)
Calibrated_H2O_sdev = Raw_H2O_sdev
n_degree = 1
FOR i=0,n_switch-1 DO BEGIN

   IF n_standard GT 1 THEN BEGIN
      ;--- OCS
      coeff = POLY_FIT(Raw_OCS_std_interp[*,i], standards.OCS, 1, /double,measure_errors=Raw_OCS_std_interp_sdev[*,i])
      IF Raw_OCS_mean[i] NE missval THEN Calibrated_OCS_mean[i]  = POLY(Raw_OCS_mean[i], coeff)
      ;--- CO2
      coeff = POLY_FIT(Raw_CO2_std_interp[*,i], standards.CO2, 1, /double,measure_errors=Raw_CO2_std_interp_sdev[*,i])
      IF Raw_CO2_mean[i] NE missval THEN Calibrated_CO2_mean[i]  = POLY(Raw_CO2_mean[i], coeff)
      ;--- H2O
      coeff = POLY_FIT(Raw_H2O_std_interp[*,i], standards.H2O, 1, /double,measure_errors=Raw_H2O_std_interp_sdev[*,i])
      IF Raw_H2O_mean[i] NE missval THEN Calibrated_H2O_mean[i]  = POLY(Raw_H2O_mean[i], coeff)
   ENDIF ELSE BEGIN
      IF Raw_OCS_mean[i] NE missval THEN Calibrated_OCS_mean[i] = Raw_OCS_mean[i] - (Raw_OCS_std_interp[0,i] - standards.OCS[0])
      IF Raw_CO2_mean[i] NE missval THEN Calibrated_CO2_mean[i] = Raw_CO2_mean[i] - (Raw_CO2_std_interp[0,i] - standards.CO2[0])
      IF Raw_H2O_mean[i] NE missval THEN Calibrated_H2O_mean[i] = Raw_H2O_mean[i] - (Raw_H2O_std_interp[0,i] - standards.H2O[0])
   ENDELSE

ENDFOR

;===================================================================================
;///////////////////////////////////////////////////////////////////////////////////
; PLOT STANDARD TIMESERIES
;///////////////////////////////////////////////////////////////////////////////////
;===================================================================================

;--- open ps file
set_plot, 'ps'
file = path_plots + data_id + '_standards_timeseries.ps'
font=!p.font
;!p.font=-1
device, filename=file, /color;, encapsulated=1, $
        ;xsize=25, ysize=16

;--- plots
nxplot = n_standard
nyplot = n_tracer
!p.multi=[0,nxplot,nyplot,0,1]
nplot = nxplot*nyplot
pos_tab = PEYL_MAKE_POS(nplot, $
                        RIGHTBORD=0.95, $
                        BOTBORD=0.03,$
                        LEFTBORD=0.05, $
                        TOPBORD=0.85,$
                        BETWEEN=0.01,$
                        LEFTFIX=0.20,$
                        RIGHTFIX=0.00,$
                        /ORDER,$
                        NCOL=nxplot)

dummy = LABEL_DATE(DATE_FORMAT=['%H:%I','%D-%M-%Y'])
FOR i=0,n_standard-1 DO BEGIN

   ixplot = i
   FOR iyplot=0,nyplot-1 do begin
   
      case iyplot of
         ;--- OCS
         0: begin
            title=standards.name[i]
            xtickformat=''
            xtickunits=''
            xtickname=replicate(' ',10)
            ytitle='OCS (ppt)'
            yraw = Raw_OCS_mean
            yraw_sd = Raw_OCS_sdev
            ycal = Calibrated_OCS_mean
            ycal_sd = Calibrated_OCS_sdev
            ycal_interp = Raw_OCS_std_interp
            ystd = standards.OCS[i]
            ystd_sd = standards.OCS_sd[i]
         end
         ;--- CO2
         1: begin
            title=''
            xtickformat=''
            xtickunits=''
            xtickname=replicate(' ',10)
            ytitle='CO!d2!n (ppm)'
            yraw = Raw_CO2_mean
            yraw_sd = Raw_CO2_sdev
            ycal = Calibrated_CO2_mean
            ycal_sd = Calibrated_CO2_sdev
            ycal_interp = Raw_CO2_std_interp
            ystd = standards.CO2[i]
            ystd_sd = standards.CO2_sd[i]
         end
         ;--- H2O
         2: begin
            title=''
            xtickformat=['LABEL_DATE','LABEL_DATE']
            xtickunits=['Time','Day']
            xtickname=replicate(' ',10)
            ytitle='H!d2!nO ('+s_multiplication+stdfont+'10!u5!n)'
            yraw = Raw_H2O_mean
            yraw_sd = Raw_H2O_sdev
            ycal = Calibrated_H2O_mean
            ycal_sd = Calibrated_H2O_sdev
            ycal_interp = Raw_H2O_std_interp
            ystd = standards.H2O[i]
            ystd_sd = standards.H2O_sd[i]
         end
      endcase
      
      ;--- draw a frame
      jj = WHERE(line_name_mean EQ standards.name[i] AND ycal ne missval)
      plot,[0],[0],title=title,position=pos_tab[*,ixplot*nyplot+iyplot],$
           ytitle=ytitle,$
           xrange=[min(date_time),max(date_time)],xstyle=1,$
           yrange=[floor(min([yraw[jj],ycal[jj]])<(ystd-ystd_sd)),$
                   ceil(max([yraw[jj],ycal[jj]])>(ystd+ystd_sd))],yticks=4,yminor=1,ystyle=0,$
           yticklen=0.01*n_standard,ytickname=ytickname,$
           xtickformat=xtickformat,xtickunits=xtickunits,xtickname=xtickname,charsize=2,/nodata

      ;--- plot true value for each cal bottle      
      polyfill,[!x.crange,REVERSE(!x.crange)],$
               [REPLICATE(ystd-ystd_sd,2),$
                REPLICATE(ystd+ystd_sd,2)],color=200

      ;--- plot raw cal tank values with error bars
      ccg_symbol,sym=2,fill=0
      oplot,[date_time_mean[jj]],[yraw[jj]],psym=8,symsize=1,color=REFORM(standards.color[i])
      mc_errplot,[date_time_mean[jj]],$
                 [(yraw[jj]-yraw_sd[jj])>!y.crange[0]],$
                 [(yraw[jj]+yraw_sd[jj])<!y.crange[1]],$
                 thick=3,width=0.00*n_standard,color=(standards.color[i])[0]

      ;--- plot interpolated values
      oplot,[date_time_mean],[ycal_interp[i,*]],psym=-8,symsize=0.3,linestyle=1,color=0
      
      ;--- plot calibrated cal tank values with error bars
      ccg_symbol,sym=1,fill=1
      oplot,[date_time_mean[jj]],[ycal[jj]],psym=8,symsize=1,color=REFORM(standards.color[i])
      mc_errplot,[date_time_mean[jj]],$
                 [(ycal[jj]-ycal_sd[jj])>!y.crange[0]],$
                 [(ycal[jj]+ycal_sd[jj])<!y.crange[1]],$
                 thick=3,width=0.00*n_standard,color=(standards.color[i])[0]
   ENDFOR
   
ENDFOR

;--- write legend ( in two steps because 2 symbol sizes used...)
mc_llegend,x=0.25,$
           y=0.95,$
           charsize=0.8,symsize=0.3,symthick=1,$
           tarr=[''],$
           carr=[0],$
           larr=[1],$
           ltarr=[1],$
           farr=[0],$
           sarr=[-2],$
           row=1,col=1,$
           llength=2,$
           incr_text=1.,$
           font=!p.font,$
           data=0
mc_llegend,x=0.25,$
           y=0.95,$
           charsize=0.8,symsize=1,symthick=1,$
           tarr=['Raw interpolated data','Raw mean data','Calibrated mean data'],$
           carr=[0,standards.color[0],standards.color[0]],$
           larr=[1,0,0],$
           ltarr=[1,1,1],$
           farr=[0,0,1],$
           sarr=[0,2,1],$
           row=1,col=3,$
           llength=2,$
           incr_text=1.,$
           font=!p.font,$
           data=0

;--- close ps file
mc_closedev
!p.font=font
!p.multi=0

;--- converts to PDF and deletes PS file (for Mac only)
cmd = 'pstopdf '+file+' -o '+(STR_SEP(file,'.ps'))[0]+'.pdf'
spawn,cmd
FILE_DELETE, file

;===================================================================================
;///////////////////////////////////////////////////////////////////////////////////
; WRITE DATA
;///////////////////////////////////////////////////////////////////////////////////
;===================================================================================

;--- read headers
datafile_out = 'data_files/calibrated_data' + dl + data_id + '_calibrated.csv'
OPENW,unit,datafile_out,/GET_LUN
printf,unit,$
       'date_time_mean,'+$
       'time_in_sec_mean,'+$
       'line_name_mean,'+$
       'N_for_mean,'+$
       'Raw_OCS_mean,'+$
       'Raw_OCS_sdev,'+$
       'Raw_CO2_mean,'+$
       'Raw_CO2_sdev,'+$
       'Raw_H2O_mean,'+$
       'Raw_H2O_sdev,'+$
       'Calibrated_OCS_mean,'+$
       'Calibrated_OCS_sdev,'+$
       'Calibrated_CO2_mean,'+$
       'Calibrated_CO2_sdev,'+$
       'Calibrated_H2O_mean,'+$
       'Calibrated_H2O_sdev'

ii = [ii_switch,n_time]
FOR i=0,n_switch-1 DO $
   printf,unit,FORMAT='((a),(",",i20),(",",a),(",",i2),12(",",f12.2))',$
          DateTime[ii[i+1]-1],$
          time_in_sec_mean[i],$
          line_name_mean[i],$
          N_for_mean[i],$
          Raw_OCS_mean[i],$
          Raw_OCS_sdev[i],$
          Raw_CO2_mean[i],$
          Raw_CO2_sdev[i],$
          Raw_H2O_mean[i],$
          Raw_H2O_sdev[i],$
          Calibrated_OCS_mean[i],$
          Calibrated_OCS_sdev[i],$
          Calibrated_CO2_mean[i],$
          Calibrated_CO2_sdev[i],$
          Calibrated_H2O_mean[i],$
          Calibrated_H2O_sdev[i]
FREE_LUN, unit


;===================================================================================
;///////////////////////////////////////////////////////////////////////////////////
; PLOT FULL TIMESERIES
;///////////////////////////////////////////////////////////////////////////////////
;===================================================================================

;--- open ps file
set_plot, 'ps'
file = path_plots + data_id + '_full_timeseries.ps'
font=!p.font
;!p.font=-1
device, filename=file, encapsulated=0, $
        /color, xsize=25, ysize=20, /landscape

;--- plots
nxplot = 1
nyplot = n_tracer
!p.multi=[0,nxplot,nyplot,0,1]
nplot = nxplot*nyplot
pos_tab = PEYL_MAKE_POS(nplot, $
                        RIGHTBORD=0.95, $
                        BOTBORD=0.05,$
                        LEFTBORD=0.00, $
                        TOPBORD=0.90,$
                        BETWEEN=-0.10,$
                        /ORDER,$
                        NCOL=nxplot)

dummy = LABEL_DATE(DATE_FORMAT=['%H:%I','%D-%M-%Y'])
ixplot=0
ii = where(time_in_sec_mean ne missval)
n_page = ceil((max(time_in_sec_mean[ii])-min(time_in_sec_mean[ii])) / sec_per_page)
FOR ipage=0,n_page -1 do begin
FOR iyplot=0,nyplot-1 do begin
   
      case iyplot of
         ;--- OCS
         0: begin
            title=''
            xtickformat=''
            xtickunits=''
            xtickname=replicate(' ',10)
            ytitle='OCS (ppt)'
            yraw    = Raw_OCS_mean
            yraw_sd = Raw_OCS_sdev
            ycal    = Calibrated_OCS_mean
            ycal_sd = Calibrated_OCS_sdev
            yraw_instant = Raw_OCS
         end
         ;--- CO2
         1: begin
            title=''
            xtickformat=''
            xtickunits=''
            xtickname=replicate(' ',10)
            ytitle='CO!d2!n (ppm)'
            yraw    = Raw_CO2_mean
            yraw_sd = Raw_CO2_sdev
            ycal    = Calibrated_CO2_mean
            ycal_sd = Calibrated_CO2_sdev
            yraw_instant = Raw_CO2
         end
         ;--- H2O
         2: begin
            title=''
            xtickformat=['LABEL_DATE','LABEL_DATE']
            xtickunits=['Time','Day']
            xtickname=replicate(' ',10)
            ytitle='H!d2!nO ('+s_multiplication+stdfont+'10!u5!n)'
            yraw    = Raw_H2O_mean
            yraw_sd = Raw_H2O_sdev
            ycal    = Calibrated_H2O_mean
            ycal_sd = Calibrated_H2O_sdev
            yraw_instant = Raw_H2O
         end
      endcase

      ;--- filter on timesteps
      tmin = (ipage  )*sec_per_page
      tmax = (ipage+1)*sec_per_page
      jj  = where((time_in_sec_mean-t0) ge tmin AND (time_in_sec_mean-t0) le tmax)
      jj2 = where((time_in_sec     -t0) ge tmin AND (time_in_sec     -t0) le tmax)

      ;--- draw a frame
      plot,[0],[0],title=title,position=pos_tab[*,ixplot*nyplot+iyplot],$
           ytitle=ytitle,$
           xrange=[min(date_time[jj2]),max(date_time[jj2])],xstyle=1,$
           yrange=[floor(min([yraw[jj],ycal[jj],yraw_instant[jj2]])),$
                   ceil(max([yraw[jj],ycal[jj],yraw_instant[jj2]]))],$
           yticks=4,yminor=1,ystyle=0,ytickformat='(i6)',yticklen=0.01*n_standard,$
           xtickformat=xtickformat,xtickunits=xtickunits,xtickname=xtickname,charsize=2,/nodata

      ;--- plot instant values
      ccg_symbol,sym=2,fill=0
      oplot,[date_time[jj2]],[yraw_instant[jj2]],psym=-8,symsize=0.3,linestyle=1,color=0
      ;if iyplot eq 2 then oplot,date_time[jj2],VICI_W[jj2]*40,color=11
      ;if iyplot eq 2 then oplot,date_time[jj2],StatusW[jj2]/1000.,color=22
      
      ;--- plot mean raw values with error bars
      ccg_symbol,sym=1,fill=1
      oplot,[date_time_mean[jj]],[yraw[jj]],psym=8,symsize=1,color=11
      mc_errplot,[date_time_mean[jj]],$
                 [(yraw[jj]-yraw_sd[jj])>!y.crange[0]],$
                 [(yraw[jj]+yraw_sd[jj])<!y.crange[1]],$
                 thick=3,width=0.00*n_standard,color=11

      ;--- plot mean calibrated values with error bars
      ccg_symbol,sym=1,fill=1
      oplot,[date_time_mean[jj]],[ycal[jj]],psym=8,symsize=1,color=23
      mc_errplot,[date_time_mean[jj]],$
                 [(ycal[jj]-ycal_sd[jj])>!y.crange[0]],$
                 [(ycal[jj]+ycal_sd[jj])<!y.crange[1]],$
                 thick=3,width=0.00*n_standard,color=23
 
      ;--- indicate acclimation and dark periods
      hh  = where(acclimation_flag[jj] eq 1,nhh)
      if nhh gt 0 then begin
         for h=0,nhh-2 do $
            polyfill,[date_time_mean[jj[hh[h]]],date_time_mean[jj[hh[h+1]]],$
                      date_time_mean[jj[hh[h+1]]],date_time_mean[jj[hh[h]]],$
                      date_time_mean[jj[hh[h]]]],$
                     !y.crange[0]+[0.05,0.05,0.97,0.97,0.05]*(!y.crange[1]-!y.crange[0]),color=8,spacing=0.2,orientation=45
         ;oplot,date_time_mean[jj[hh]],replicate(!y.crange[0]+0.05*(!y.crange[1]-!y.crange[0]),nhh),color=0,psym=6,symsize=0.3
      endif

      ;--- write line name
      xyouts,date_time_mean[jj],yraw[jj],'!c!c'+line_name_mean[jj],color=11,$
             alignment=0.5,charsize=0.5

      ;--- write legend ( in two steps because 2 symbol sizes used...)
      IF iyplot eq 0 then begin
         mc_llegend,x=!x.crange[0] + 0.15*(!x.crange[1]-!x.crange[0]),$
                    y=!y.crange[0] + 1.10*(!y.crange[1]-!y.crange[0]),$
                    charsize=0.8,symsize=0.3,symthick=1,$
                    tarr=[''],$
                    carr=[0],$
                    larr=[1],$
                    ltarr=[1],$
                    farr=[0],$
                    sarr=[-2],$
                    row=1,col=1,$
                    llength=2,$
                    incr_text=1.,$
                    font=!p.font,$
                    data=1
         mc_llegend,x=!x.crange[0] + 0.15*(!x.crange[1]-!x.crange[0]),$
                    y=!y.crange[0] + 1.10*(!y.crange[1]-!y.crange[0]),$
                    charsize=0.8,symsize=1,symthick=1,$
                    tarr=['Raw data','Raw mean data','Calibrated mean data'],$
                    carr=[0,11,23],$
                    larr=[1,0,0],$
                    ltarr=[1,1,1],$
                    farr=[0,1,1],$
                    sarr=[0,1,1],$
                    row=1,col=3,$
                    llength=2,$
                    incr_text=1.,$
                    font=!p.font,$
                    data=1
      ENDIF
      
ENDFOR
ENDFOR

;--- close ps file
mc_closedev
!p.font=font
!p.multi=0

;--- converts to PDF and deletes PS file (for Mac only)
cmd = 'pstopdf '+file+' -o '+(STR_SEP(file,'.ps'))[0]+'.pdf'
spawn,cmd
FILE_DELETE, file

;===================================================================================
;///////////////////////////////////////////////////////////////////////////////////
; COMPUTE FLUXES
;///////////////////////////////////////////////////////////////////////////////////
;===================================================================================

ii = where(line_name_mean EQ 'out' AND SHIFT(line_name_mean,infirst) EQ 'in' AND $
           Calibrated_OCS_mean NE missval AND SHIFT(Calibrated_OCS_mean,infirst) NE missval,n_flux)
;;--- little checks
if infirst eq +1 then begin
   if ii[0] eq 0 then ii = ii[1:n_flux-1] ;--- case where 'out' measured at begining but with no previous 'in'
endif else begin
   if ii[n_flux-1] eq n_switch-1 then ii = ii[0:n_flux-2] ;--- case where 'out' measured at end but with no subsequent 'in'
endelse
n_flux = n_elements(ii)
;;...
Time_flux   = DBLARR(n_flux)
Flux_OCS    = REPLICATE(missval, n_flux)
Flux_OCS_sd = REPLICATE(missval, n_flux)
Conc_OCS    = REPLICATE(missval, n_flux)
Conc_OCS_sd = REPLICATE(missval, n_flux)
Conc_OCS_in    = REPLICATE(missval, n_flux)
Conc_OCS_in_sd = REPLICATE(missval, n_flux)
Flux_CO2    = REPLICATE(missval, n_flux)
Flux_CO2_sd = REPLICATE(missval, n_flux)
Conc_CO2    = REPLICATE(missval, n_flux)
Conc_CO2_sd = REPLICATE(missval, n_flux)
Conc_CO2_in    = REPLICATE(missval, n_flux)
Conc_CO2_in_sd = REPLICATE(missval, n_flux)
Flux_H2O    = REPLICATE(missval, n_flux)
Flux_H2O_sd = REPLICATE(missval, n_flux)
Conc_H2O    = REPLICATE(missval, n_flux)
Conc_H2O_sd = REPLICATE(missval, n_flux)
Chamber_id  = REPLICATE(missval, n_flux)
factor      = 101325./(r_gas*tk0) * (chamber_flow*1e-3/60.) / soil_area
FOR i=0,n_flux-1 DO BEGIN
   Time_flux[i]   = date_time_mean[ii[i]]
   Conc_OCS[i]    =  Calibrated_OCS_mean[ii[i]] ;--- ppt
   Conc_OCS_sd[i] =  Calibrated_OCS_sdev[ii[i]] ;--- ppt
   Conc_OCS_in[i]    =  Calibrated_OCS_mean[ii[i]-infirst] ;--- ppt
   Conc_OCS_in_sd[i] =  Calibrated_OCS_sdev[ii[i]-infirst] ;--- ppt
   Conc_CO2[i]    =  Calibrated_CO2_mean[ii[i]] ;--- ppm
   Conc_CO2_sd[i] =  Calibrated_CO2_sdev[ii[i]] ;--- ppm
   Conc_CO2_in[i]    =  Calibrated_CO2_mean[ii[i]-infirst] ;--- ppm
   Conc_CO2_in_sd[i] =  Calibrated_CO2_sdev[ii[i]-infirst] ;--- ppm
   Conc_H2O[i]    =  Calibrated_H2O_mean[ii[i]] ;--- ppm
   Conc_H2O_sd[i] =  Calibrated_H2O_sdev[ii[i]] ;--- ppm
   Chamber_id[i] = FIX(VICI_W_mean[ii[i]])
   IF (ii[i]-infirst) le (n_switch-1) THEN BEGIN
      Flux_OCS[i]    = (Calibrated_OCS_mean[ii[i]] -  Calibrated_OCS_mean[ii[i]-infirst]) * factor       ;--- pmol/m2/s
      Flux_OCS_sd[i] = (Calibrated_OCS_sdev[ii[i]] +  Calibrated_OCS_sdev[ii[i]-infirst]) * factor       ;--- pmol/m2/s
      Flux_CO2[i]    = (Calibrated_CO2_mean[ii[i]] -  Calibrated_CO2_mean[ii[i]-infirst]) * factor       ;--- umol/m2/s
      Flux_CO2_sd[i] = (Calibrated_CO2_sdev[ii[i]] +  Calibrated_CO2_sdev[ii[i]-infirst]) * factor       ;--- umol/m2/s
      Flux_H2O[i]    = (Calibrated_H2O_mean[ii[i]] -  Calibrated_H2O_mean[ii[i]-infirst]) * factor/1e3   ;--- mmol/m2/s
      Flux_H2O_sd[i]  = (Calibrated_H2O_sdev[ii[i]] +  Calibrated_H2O_sdev[ii[i]-infirst]) * factor/1e3  ;--- mmol/m2/s
   ENDIF
ENDFOR

;===================================================================================
;///////////////////////////////////////////////////////////////////////////////////
; PLOT FLUX TIMESERIES
;///////////////////////////////////////////////////////////////////////////////////
;===================================================================================

;--- open ps file
set_plot, 'ps'
file = path_plots + data_id + '_flux_timeseries.ps'
font=!p.font
;!p.font=-1
device, filename=file;, encapsulated=1, $
        ;/color, xsize=22, ysize=20;, /landscape

;--- plots
nxplot = 1
nyplot = n_tracer
!p.multi=[0,nxplot,nyplot,0,1]
nplot = nxplot*nyplot
pos_tab = PEYL_MAKE_POS(nplot, $
                        RIGHTBORD=0.96, $
                        BOTBORD=0.05,$
                        LEFTBORD=0.0, $
                        TOPBORD=0.96,$
                        BETWEEN=-0.10,$
                        /ORDER,$
                        NCOL=nxplot)

dummy = LABEL_DATE(DATE_FORMAT=['%H:%I','%D-%M-%Y'])
FOR iyplot=0,nyplot-1 do begin
   
      case iyplot of
         ;--- OCS
         0: begin
            title=''
            xtickformat=''
            xtickunits=''
            xtickname=replicate(' ',10)
            ytitle=stdfont+'OCS flux (pmol m!u-2!n s!u-1!n)'
            ycal    = Flux_OCS
            yrange = [-4., 0.5]
         end
         ;--- CO2
         1: begin
            title=''
            xtickformat=''
            xtickunits=''
            xtickname=replicate(' ',10)
            ytitle=stdfont+'CO!d2!n flux ('+s_micro+stdfont+'mol m!u-2!n s!u-1!n)'
            ycal    = Flux_CO2
            yrange = [-2.,2.]
         end
         ;--- H2O
         2: begin
            title=''
            xtickformat=['LABEL_DATE','LABEL_DATE']
            xtickunits=['Time','Day']
            xtickname=replicate(' ',10)
            ytitle=stdfont+'H!d2!nO flux (mmol m!u-2!n s!u-1!n)'
            ycal    = Flux_H2O
            yrange = [-0.8,0.8]
         end
      endcase
      yrange = ogee_Extrema(ycal,min_percent=1,max_percent=99)
      yrange[1] = max([yrange[1],yrange[0]+0.5])
      
      ;--- draw a frame
      ii = where(ycal ne missval)
      jj = where(abs(ycal[ii]-mean(ycal[ii])) le 5.*stddev(ycal[ii])) 
      ;yrange=[min(ycal[ii[jj]]),max(ycal[ii[jj]])]
      plot,[0],[0],title=title,position=pos_tab[*,iyplot],$
           ytitle=ytitle,$
           xrange=[min(Time_Flux),max(Time_Flux)],xstyle=1,$
           yrange=yrange,$
           ytickformat='(f5.1)',yminor=1,ystyle=0,yticklen=0.01*n_standard,$
           xtickformat=xtickformat,xtickunits=xtickunits,xtickname=xtickname,charsize=2,/nodata

      ;--- plot mean calibrated values
      ccg_symbol,sym=1,fill=0
      oplot,Time_Flux,ycal,psym=-8,symsize=1.,color=0
      ccg_symbol,sym=1,fill=1
      ii = where(Chamber_id eq blank_id)
      oplot,Time_Flux[ii],ycal[ii],psym=8,symsize=1.,color=31
      for ich=0,n_elements(algae_id)-1 do begin
         ii = where(Chamber_id eq algae_id[ich],nii)
         if nii gt 0 then oplot,Time_Flux[ii],ycal[ii],psym=8,symsize=1.,color=63
      endfor     
ENDFOR

;--- close ps file
mc_closedev
!p.font=font
!p.multi=0

;--- converts to PDF and deletes PS file (for Mac only)
cmd = 'pstopdf '+file+' -o '+(STR_SEP(file,'.ps'))[0]+'.pdf'
spawn,cmd
FILE_DELETE, file

;===================================================================================
;///////////////////////////////////////////////////////////////////////////////////
; WRITE DATA
;///////////////////////////////////////////////////////////////////////////////////
;===================================================================================

;--- read headers
datafile_out = 'data_files/calibrated_data' + dl + data_id + '_fluxes.csv'
if flag_demo eq 0 then OPENW,unit,datafile_out,/GET_LUN
if flag_demo eq 1 then cmd_print='print,' else cmd_print='printf,unit,'
cmd = cmd_print+$
      '"Time_flux,"+'+$
      '"Chamber_id,"+'+$
      '"Flux_OCS,"+'+$
      '"Flux_OCS_sd,"+'+$
      '"Conc_OCS,"+'+$
      '"Conc_OCS_sd,"+'+$
      '"Conc_OCS_in,"+'+$
      '"Conc_OCS_in_sd,"+'+$
      '"Flux_CO2,"+'+$
      '"Flux_CO2_sd,"+'+$
      '"Conc_CO2,"+'+$
      '"Conc_CO2_sd,"+'+$
      '"Conc_CO2_in,"+'+$
      '"Conc_CO2_in_sd,"+'+$
      '"Flux_H2O,"+'+$
      '"Conc_H2O"'
res = execute(cmd)

cmd = cmd_print+$
   'FORMAT="(f20.8,'','',i6,14('','',f12.2))",'+$
          'Time_flux[i],'+$
          'Chamber_id[i],'+$
          'Flux_OCS[i],'+$
          'Flux_OCS_sd[i],'+$
          'Conc_OCS[i],'+$
          'Conc_OCS_sd[i],'+$
          'Conc_OCS_in[i],'+$
          'Conc_OCS_in_sd[i],'+$
          'Flux_CO2[i],'+$
          'Flux_CO2_sd[i],'+$
          'Conc_CO2[i],'+$
          'Conc_CO2_sd[i],'+$
          'Conc_CO2_in[i],'+$
          'Conc_CO2_in_sd[i],'+$
          'Flux_H2O[i],'+$
          'Conc_H2O[i]'
FOR i=0,n_flux-1 DO res = execute(cmd)
if flag_demo eq 0 then FREE_LUN, unit

;===================================================================================
;///////////////////////////////////////////////////////////////////////////////////
; COMPUTE MEAN FLUXES PER POT
;///////////////////////////////////////////////////////////////////////////////////
;===================================================================================

ii = where(Chamber_id-SHIFT(Chamber_id,+1) NE 0, n_final)
Time_flux_final = DBLARR(n_final)
Flux_OCS_final = REPLICATE(missval, n_final)
Flux_OCS_sd_final = REPLICATE(missval, n_final)
Conc_OCS_final = REPLICATE(missval, n_final)
Conc_OCS_sd_final = REPLICATE(missval, n_final)
Conc_OCS_in_final = REPLICATE(missval, n_final)
Conc_OCS_in_sd_final = REPLICATE(missval, n_final)
Flux_CO2_final = REPLICATE(missval, n_final)
Flux_CO2_sd_final = REPLICATE(missval, n_final)
Conc_CO2_final = REPLICATE(missval, n_final)
Conc_CO2_sd_final = REPLICATE(missval, n_final)
Conc_CO2_in_final = REPLICATE(missval, n_final)
Conc_CO2_in_sd_final = REPLICATE(missval, n_final)
Flux_H2O_final = REPLICATE(missval, n_final)
Flux_H2O_sd_final = REPLICATE(missval, n_final)
Conc_H2O_final = REPLICATE(missval, n_final)
Conc_H2O_sd_final = REPLICATE(missval, n_final)
Chamber_id_final = Chamber_id[ii]
FOR i=0,n_final-1 DO BEGIN
   imin=ii[i]
   if i lt n_final-1 then imax=ii[i+1]-1 else imax=n_flux-1
   ;if i lt n_final-1 then imax=ii[i+1]-1 else imax=ii[n_flux-1] ;--- need to be checked
   if imax gt imin then begin
      Time_flux_final[i]      = MEAN(Time_flux[imin:imax])
      Conc_OCS_final[i]       = MEAN(Conc_OCS[imin:imax])
      Conc_OCS_sd_final[i]    = MEAN(Conc_OCS_sd[imin:imax])
      Conc_OCS_in_final[i]    = MEAN(Conc_OCS_in[imin:imax])
      Conc_OCS_in_sd_final[i] = MEAN(Conc_OCS_in_sd[imin:imax])
      Conc_CO2_final[i]       = MEAN(Conc_CO2[imin:imax])
      Conc_CO2_sd_final[i]    = MEAN(Conc_CO2_sd[imin:imax])
      Conc_CO2_in_final[i]    = MEAN(Conc_CO2_in[imin:imax])
      Conc_CO2_in_sd_final[i] = MEAN(Conc_CO2_in_sd[imin:imax])
      Conc_H2O_final[i]       = MEAN(Conc_H2O[imin:imax])
      Conc_H2O_sd_final[i]    = MEAN(Conc_H2O_sd[imin:imax])
      Flux_OCS_final[i]       = MEAN(Flux_OCS[imin:imax])
      Flux_OCS_sd_final[i]    = MEAN(Flux_OCS_sd[imin:imax])
      Flux_CO2_final[i]       = MEAN(Flux_CO2[imin:imax])
      Flux_CO2_sd_final[i]    = MEAN(Flux_CO2_sd[imin:imax])
      Flux_H2O_final[i]       = MEAN(Flux_H2O[imin:imax])
      Flux_H2O_sd_final[i]    = MEAN(Flux_H2O_sd[imin:imax])
   endif
ENDFOR

;===================================================================================
;///////////////////////////////////////////////////////////////////////////////////
; WRITE DATA
;///////////////////////////////////////////////////////////////////////////////////
;===================================================================================

;--- read headers
datafile_out = 'data_files/calibrated_data' + dl + data_id + '_mean_fluxes.csv'
if flag_demo eq 0 then OPENW,unit,datafile_out,/GET_LUN
if flag_demo eq 1 then cmd_print='print,' else cmd_print='printf,unit,'
cmd = cmd_print+$
      '"Time_flux_final,"+'+$
      '"Chamber_id_final,"+'+$
      '"Chamber_flow_lpm,"+'+$
      '"Flux_OCS_final,"+'+$
      '"Flux_OCS_sd_final,"+'+$
      '"Conc_OCS_final,"+'+$
      '"Conc_OCS_sd_final,"+'+$
      '"Conc_OCS_in_final,"+'+$
      '"Conc_OCS_in_sd_final,"+'+$
      '"Flux_CO2_final,"+'+$
      '"Flux_CO2_sd_final,"+'+$
      '"Conc_CO2_final,"+'+$
      '"Conc_CO2_sd_final,"+'+$
      '"Conc_CO2_in_final,"+'+$
      '"Conc_CO2_in_sd_final"'
res = execute(cmd)

cmd = cmd_print+$
      'FORMAT="(f20.8,'','',i6,13('','',f8.3))",'+$
          'Time_flux_final[i],'+$
          'Chamber_id_final[i],'+$
          'Chamber_flow,'+$
          'Flux_OCS_final[i],'+$
          'Flux_OCS_sd_final[i],'+$
          'Conc_OCS_final[i],'+$
          'Conc_OCS_sd_final[i],'+$
          'Conc_OCS_in_final[i],'+$
          'Conc_OCS_in_sd_final[i],'+$
          'Flux_CO2_final[i],'+$
          'Flux_CO2_sd_final[i],'+$
          'Conc_CO2_final[i],'+$
          'Conc_CO2_sd_final[i],'+$
          'Conc_CO2_in_final[i],'+$
          'Conc_CO2_in_sd_final[i]'
FOR i=0,n_final-1 DO res = execute(cmd)
if flag_demo eq 0 then FREE_LUN, unit

stop
stop

END
